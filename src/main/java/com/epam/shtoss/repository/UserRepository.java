package com.epam.shtoss.repository;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.User;
import com.epam.shtoss.model.builders.Builder;
import com.epam.shtoss.model.builders.UserBuilder;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class UserRepository extends AbstractRepository<User> {

    public UserRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected Builder<User> getBuilder() {
        return new UserBuilder();
    }

    @Override
    protected void setParameters(PreparedStatement preparedStatement, List<Object> parameters) throws RepositoryException {
        String command = (String)parameters.get(0);
        switch (command) {
            case ("login"): {
                try {

                    preparedStatement.setString(1, (String) parameters.get(1));
                    preparedStatement.setString(2, (String) parameters.get(2));
                } catch (SQLException e) {
                    throw new RepositoryException(e.getMessage(), e);
                }
            }
            break;
            case ("showAllUsers"): {

            }
            break;

            case ("update"): {
                try {

                    preparedStatement.setBoolean(1, (Boolean) parameters.get(1));
                    preparedStatement.setString(2, (String) parameters.get(2));
                } catch (SQLException e) {
                    throw new RepositoryException(e.getMessage(), e);
                }
            }
            break;

            case ("getUser"): {
                try {

                    preparedStatement.setLong(1, (Long) parameters.get(1));
                } catch (SQLException e) {
                    throw new RepositoryException(e.getMessage(), e);
                }
            }
            break;

            case ("updateAfterGame"): {
                try {

                    preparedStatement.setBigDecimal(1, (BigDecimal) parameters.get(1));
                    preparedStatement.setInt(2, (Integer) parameters.get(2));
                    preparedStatement.setLong(3, (Long) parameters.get(3));
                } catch (SQLException e) {
                    throw new RepositoryException(e.getMessage(), e);
                }
            }
            break;

            case ("updateAfterReplenish"): {
                try {

                    preparedStatement.setBigDecimal(1, (BigDecimal) parameters.get(1));
                    preparedStatement.setLong(2, (Long) parameters.get(2));
                } catch (SQLException e) {
                    throw new RepositoryException(e.getMessage(), e);
                }
            }
            break;

            case ("add"): {
                try {

                    preparedStatement.setString(1, (String) parameters.get(1));
                    preparedStatement.setString(2, (String) parameters.get(2));
                    preparedStatement.setString(3, (String) parameters.get(3));
                    preparedStatement.setBoolean(4, (Boolean) parameters.get(4));
                    preparedStatement.setString(5, (String) parameters.get(5));
                    preparedStatement.setBoolean(6, (Boolean) parameters.get(6));
                    preparedStatement.setBigDecimal(7, (BigDecimal) parameters.get(7));
                    preparedStatement.setInt(8, (Integer) parameters.get(8));
                } catch (SQLException e) {
                    throw new RepositoryException(e.getMessage(), e);
                }
            }
            break;
        }

    }
}
