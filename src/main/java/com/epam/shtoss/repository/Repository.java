package com.epam.shtoss.repository;
import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.Entity;
import com.epam.shtoss.specification.SqlSpecification;

import java.util.List;
import java.util.Optional;

public interface Repository<T extends Entity> {
    /**
     * provides all updateQuery operations
     * @param sqlSpecification
     * @return boolean
     * @throws RepositoryException
     */
    boolean add(SqlSpecification sqlSpecification) throws RepositoryException;
    void remove();
    void update();

    List<T> query(SqlSpecification sqlSpecification) throws RepositoryException;
    Optional<T> queryBySingleResult(SqlSpecification sqlSpecification) throws RepositoryException;

}
