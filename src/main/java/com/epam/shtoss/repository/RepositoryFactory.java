package com.epam.shtoss.repository;

import com.epam.shtoss.connection.ConnectionPool;
import com.epam.shtoss.exceptions.ConnectionException;
import com.epam.shtoss.exceptions.ConnectionPoolException;

import java.sql.Connection;

public class RepositoryFactory implements AutoCloseable {

    private Connection connection;

    public RepositoryFactory() throws ConnectionPoolException, ConnectionException {
        connection = ConnectionPool.getInstance().getConnection();

    }
    public UserRepository getUserRepository() {
        return new UserRepository(connection);
    }

    public NewsRepository getNewsRepository() {
        return new NewsRepository(connection);
    }

    public GameRepository getGameRepository() {
        return new GameRepository(connection);
    }

    public MessageRepository getMessageRepository() {
        return new MessageRepository(connection);
    }


    @Override
    public void close() throws ConnectionPoolException {
        ConnectionPool.getInstance().releaseConnection(connection);
    }
}
