package com.epam.shtoss.repository;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.News;
import com.epam.shtoss.model.builders.Builder;
import com.epam.shtoss.model.builders.NewsBuilder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class NewsRepository extends AbstractRepository<News> {

    public NewsRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected Builder<News> getBuilder() {
        return new NewsBuilder();
    }

    @Override
    protected void setParameters(PreparedStatement preparedStatement, List<Object> parameters) throws RepositoryException {
        String command = (String)parameters.get(0);
        switch (command) {
            case ("insert"): {
                try {
                    preparedStatement.setString(1, (String) parameters.get(1));
                    preparedStatement.setString(2, (String) parameters.get(2));
                    preparedStatement.setString(3, (String) parameters.get(3));

                } catch (SQLException e) {
                    throw new RepositoryException(e.getMessage(), e);
                }
            }
            break;
            case ("showAllNews"): {

            }
            break;
        }
    }
}
