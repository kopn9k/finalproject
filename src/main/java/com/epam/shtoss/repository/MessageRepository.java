package com.epam.shtoss.repository;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.Message;
import com.epam.shtoss.model.builders.Builder;
import com.epam.shtoss.model.builders.MessageBuilder;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class MessageRepository extends AbstractRepository<Message> {

    public MessageRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected Builder<Message> getBuilder() {
        return new MessageBuilder();
    }

    @Override
    protected void setParameters(PreparedStatement preparedStatement, List<Object> parameters) throws RepositoryException {
        String command = (String)parameters.get(0);
        switch (command) {
            case ("insertMessage"): {
                try {

                    preparedStatement.setLong(1, (Long) parameters.get(1));
                    preparedStatement.setString(2, (String) parameters.get(2));
                    preparedStatement.setString(3, (String) parameters.get(3));
                    preparedStatement.setString(4, (String) parameters.get(4));

                } catch (SQLException e) {
                    throw new RepositoryException(e.getMessage(), e);
                }
            }
            break;
            case ("showMessages"): {
                try {

                    preparedStatement.setString(1, (String) parameters.get(1));
                } catch (SQLException e) {
                    throw new RepositoryException(e.getMessage(), e);
                }
            }
            break;
        }
    }
}
