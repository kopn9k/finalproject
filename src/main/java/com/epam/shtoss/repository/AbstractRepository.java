package com.epam.shtoss.repository;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.Entity;
import com.epam.shtoss.model.builders.Builder;
import com.epam.shtoss.specification.SqlSpecification;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<T extends Entity> implements Repository {

    private Connection connection;

    public AbstractRepository(Connection connection) {
        this.connection = connection;
    }
    @Override
    public boolean add(SqlSpecification sqlSpecification) throws RepositoryException {
        String template = sqlSpecification.toSqlClauses();
        List<Object> parameters = sqlSpecification.getParameters();
        PreparedStatement preparedStatement;
        boolean result = false;
        try {
            preparedStatement = connection.prepareStatement(template);
            setParameters(preparedStatement, parameters);
            int amountOfModifyingRows = preparedStatement.executeUpdate();
            if (amountOfModifyingRows == 1) {
             result = true;
            }

        } catch (SQLException e) {
            throw new RepositoryException(e.getMessage(), e);
        }
        return result;
    }

    @Override
    public void remove() {

    }

    @Override
    public void update() {

    }

    @Override
    public List<T> query(SqlSpecification sqlSpecification) throws RepositoryException{
        String template = sqlSpecification.toSqlClauses();
        Builder<T> builder = getBuilder();
        List<Object> parameters = sqlSpecification.getParameters();
        return executeQuery(builder, template, parameters);

    }

    protected List<T> executeQuery(Builder<T> builder, String template, List<Object> parameters) throws RepositoryException {
        List<T> result;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(template);
            setParameters(preparedStatement, parameters);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = buildResults(resultSet, builder);
        } catch (SQLException e) {
            throw new RepositoryException(e.getMessage(), e);
        }
        return result;
    }

    private List<T> buildResults(ResultSet resultSet, Builder<T> builder) throws RepositoryException {
        List<T> result = new ArrayList<>();
        try {
            while (resultSet.next()) {
                T entity = builder.build(resultSet);
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new RepositoryException(e.getMessage(), e);
        }
        return result;
    }

    abstract protected Builder<T> getBuilder();
    abstract protected void setParameters(PreparedStatement preparedStatement, List<Object> parameters) throws RepositoryException;

    @Override
    public Optional queryBySingleResult(SqlSpecification sqlSpecification) throws RepositoryException {
        List<T> queryResult = query(sqlSpecification);
        if(queryResult.size() == 1) {
            T value = queryResult.get(0);
            return Optional.of(value);
        }
        return Optional.empty();
    }
}
