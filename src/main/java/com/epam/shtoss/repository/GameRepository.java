package com.epam.shtoss.repository;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.Game;
import com.epam.shtoss.model.builders.Builder;
import com.epam.shtoss.model.builders.GameBuilder;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class GameRepository extends AbstractRepository<Game> {

    public GameRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected Builder<Game> getBuilder() {
        return new GameBuilder();
    }

    @Override
    protected void setParameters(PreparedStatement preparedStatement, List<Object> parameters) throws RepositoryException {
        String command = (String)parameters.get(0);
        switch (command) {
            case ("add"): {
                try {
                    preparedStatement.setLong(1, (Long) parameters.get(1));
                    preparedStatement.setBigDecimal(2, (BigDecimal) parameters.get(2));
                    preparedStatement.setBoolean(3, (Boolean) parameters.get(3));
                    preparedStatement.setString(4, (String) parameters.get(4));

                } catch (SQLException e) {
                    throw new RepositoryException(e.getMessage(), e);
                }
            }
            break;
            case ("showAllGames"): {

            }
            break;
        }
    }
}
