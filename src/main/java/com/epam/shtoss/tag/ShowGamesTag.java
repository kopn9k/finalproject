package com.epam.shtoss.tag;

import com.epam.shtoss.model.Game;
import com.epam.shtoss.model.News;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.List;

@SuppressWarnings("serial")
public class ShowGamesTag extends TagSupport {

    private List<Game> games;
    private int page;
    private int lastPage;
    private int amountOfGamesOnPage = 10;

    public void setGames(List<Game> games) {
        this.games = games;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public int doStartTag() throws JspException {
        double amountOfGames = games.size();
        lastPage = (int) Math.ceil(amountOfGames / amountOfGamesOnPage);
        int lastGame = page * amountOfGamesOnPage;
        if (lastGame > games.size()) {
            lastGame = games.size();
        }
        int firstGame = page * amountOfGamesOnPage - amountOfGamesOnPage;

        for (int i = firstGame; i < lastGame; i++) {
            Game game = games.get(i);
            try {
                pageContext.getOut().write("<tr><td>" + game.getAuthor() + "</td><td>" + game.getBank() + "</td><td>" + game.isWin() + "</td><td>" + game.getDate() + "</td></tr>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        int nextPage = page + 1;
        int previousPage = page - 1;
        if (lastPage == 1) {

        }else if(page == 1) {
            try {
                pageContext.getOut().write("<a href=/shtoss/controller?command=goGamePage&page=" + nextPage + ">next</a>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if(page == lastPage) {
            try {
                pageContext.getOut().write("<a href=/shtoss/controller?command=goGamePage&page=" + previousPage + ">previous</a>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                pageContext.getOut().write("<a href=/shtoss/controller?command=goGamePage&page=" + previousPage + ">previous</a><br>");
                pageContext.getOut().write("<a href=/shtoss/controller?command=goGamePage&page=" + nextPage + ">next</a>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}