package com.epam.shtoss.tag;

import com.epam.shtoss.model.News;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.List;

@SuppressWarnings("serial")
public class ShowNewsTag extends TagSupport {

    private List<News> news;
    private int page;
    private int lastPage;
    private int amountOfNewsOnPage = 3;

    public void setNews(List<News> news) {
        this.news = news;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public int doStartTag() throws JspException {
        double amountOfNews = news.size();
        lastPage = (int)Math.ceil(amountOfNews/amountOfNewsOnPage);
        int lastNews = page*amountOfNewsOnPage;
        if (lastNews > news.size()) {
            lastNews = news.size();
        }
        int firstNews = page*amountOfNewsOnPage-amountOfNewsOnPage;

        for (int i = firstNews; i < lastNews; i++) {
            News aNew = news.get(i);
            try {
                pageContext.getOut().write("<div><p>" + aNew.getDate() + "</p><p>" + aNew.getText() + "</p><p>" + aNew.getAuthor() + "</p></div>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        int nextPage = page + 1;
        int previousPage = page - 1;
        if (lastPage == 1) {

        }else if(page == 1) {
            try {
                pageContext.getOut().write("<a href=/shtoss/controller?command=showNews&page=" + nextPage + ">next</a>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if(lastPage == 1) {

        } else if(page == lastPage) {
            try {
                pageContext.getOut().write("<a href=/shtoss/controller?command=showNews&page=" + previousPage + ">previous</a>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                pageContext.getOut().write("<a href=/shtoss/controller?command=showNews&page=" + previousPage + ">previous</a><br>");
                pageContext.getOut().write("<a href=/shtoss/controller?command=showNews&page=" + nextPage + ">next</a>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
