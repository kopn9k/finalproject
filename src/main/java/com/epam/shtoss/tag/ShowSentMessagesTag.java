package com.epam.shtoss.tag;

import com.epam.shtoss.model.Game;
import com.epam.shtoss.model.Message;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.List;

@SuppressWarnings("serial")
public class ShowSentMessagesTag extends TagSupport {

    private List<Message> messages;
    private int page;
    private int lastPage;
    private int amountOfMessagesOnPage = 5;

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public int doStartTag() throws JspException {
        double amountOfMessages = messages.size();
        lastPage = (int)Math.ceil(amountOfMessages/ amountOfMessagesOnPage);
        int lastMessage = page* amountOfMessagesOnPage;
        if (lastMessage > messages.size()) {
            lastMessage = messages.size();
        }
        int firstMessage = page* amountOfMessagesOnPage - amountOfMessagesOnPage;

        for (int i = firstMessage; i < lastMessage; i++) {
            Message message = messages.get(i);
            try {
                pageContext.getOut().write("<tr><td>" + message.getAuthor() + "</td><td>" + message.getRecipient() + "</td><td>" + message.getMessage() + "</td><td>" + message.getDate() + "</td></tr>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        int nextPage = page + 1;
        int previousPage = page - 1;
        if (lastPage == 1) {

        } else if(page == 1) {
            try {
                pageContext.getOut().write("<a href=/shtoss/controller?command=showSentMessages&page=" + nextPage + ">next</a>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if(page == lastPage) {
            try {
                pageContext.getOut().write("<a href=/shtoss/controller?command=showSentMessages&page=" + previousPage + ">previous</a>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                pageContext.getOut().write("<a href=/shtoss/controller?command=showSentMessages&page=" + previousPage + ">previous</a><br>");
                pageContext.getOut().write("<a href=/shtoss/controller?command=showSentMessages&page=" + nextPage + ">next</a>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}