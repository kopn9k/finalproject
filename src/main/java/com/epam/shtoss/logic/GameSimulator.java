package com.epam.shtoss.logic;

import java.util.HashMap;
import java.util.Map;

/**
 * prototype of the game, return result of one played game
 */
public class GameSimulator {

    private ValueGenerator valueGenerator;

    public GameSimulator(ValueGenerator valueGenerator) {
        this.valueGenerator = valueGenerator;
    }
    public boolean playAGame(String card) {
        Map<String, Integer> deck = createCardDeck();
        for(Map.Entry<String, Integer> entry : deck.entrySet()) {
            String key = entry.getKey();
            Integer value = valueGenerator.generateValue();
            entry.setValue(value);
        }
        int result = deck.get(card);
        return result == 1;
    }

    public boolean isCard(String card) {
        Map<String, Integer> deck = createCardDeck();
        return deck.containsKey(card);
    }

    private Map<String, Integer> createCardDeck() {
        Map<String, Integer> deck = new HashMap<>();
        deck.put("6H", 0);
        deck.put("7H", 0);
        deck.put("8H", 0);
        deck.put("9H", 0);
        deck.put("10H", 0);
        deck.put("JH", 0);
        deck.put("QH", 0);
        deck.put("KH", 0);
        deck.put("AH", 0);
        deck.put("6D", 0);
        deck.put("7D", 0);
        deck.put("8D", 0);
        deck.put("9D", 0);
        deck.put("10D", 0);
        deck.put("JD", 0);
        deck.put("QD", 0);
        deck.put("KD", 0);
        deck.put("AD", 0);
        deck.put("6S", 0);
        deck.put("7S", 0);
        deck.put("8S", 0);
        deck.put("9S", 0);
        deck.put("10S", 0);
        deck.put("JS", 0);
        deck.put("QS", 0);
        deck.put("KS", 0);
        deck.put("AS", 0);
        deck.put("6C", 0);
        deck.put("7C", 0);
        deck.put("8C", 0);
        deck.put("9C", 0);
        deck.put("10C", 0);
        deck.put("JC", 0);
        deck.put("QC", 0);
        deck.put("KC", 0);
        deck.put("AC", 0);
        return deck;
    }
}
