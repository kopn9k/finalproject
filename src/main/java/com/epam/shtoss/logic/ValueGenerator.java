package com.epam.shtoss.logic;

public class ValueGenerator {

    private int value;

    public ValueGenerator(int value) {
        this.value = value;
    }

    public ValueGenerator() {

    }

    public int generateValue() {
        return value;
    }
}
