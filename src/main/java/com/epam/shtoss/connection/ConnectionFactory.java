package com.epam.shtoss.connection;

import com.epam.shtoss.exceptions.ConnectionException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * create connections and read properties from file
 */

public class ConnectionFactory {
    private final static String DRIVER = "com.mysql.jdbc.Driver";
    private final static String URL = "db.url";
    private final static String USER = "db.login";
    private final static String PASSWORD = "db.password";

    public Connection createConnection(String propertyPath) {
        Properties connectionProperties = readProperties(propertyPath);
        String url = connectionProperties.getProperty(URL);
        String user = connectionProperties.getProperty(USER);
        String password = connectionProperties.getProperty(PASSWORD);

        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            throw new ConnectionException(e.getMessage(), e);
        }
        try {
            return DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            throw new ConnectionException(e.getMessage(), e);
        }
    }

    public Properties readProperties(String propertyPath) {
        Properties properties = new Properties();
        try(InputStream inputStream = ConnectionFactory.class.getResourceAsStream(propertyPath)) {
            properties.load(inputStream);
        } catch (IOException e) {
            throw new ConnectionException(e.getMessage(), e);
        }
        return properties;
    }
}
