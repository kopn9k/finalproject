package com.epam.shtoss.connection;

import com.epam.shtoss.exceptions.ConnectionException;
import com.epam.shtoss.exceptions.ConnectionPoolException;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.*;

/**
 * provider synchronized Connection pool
 */

public class ConnectionPool {

    private static ConnectionPool instance;
    private static AtomicBoolean isInitialized = new AtomicBoolean(false);
    private Queue<Connection> connections;
    private final static Lock LOCK =  new ReentrantLock();
    private final static String PROPERTY_PATH = "/config.properties";
    private static final int POOL_SIZE = 7;
    private final Semaphore semaphore = new Semaphore(POOL_SIZE, true);
    private final ConnectionFactory connectionFactory = new ConnectionFactory();

    private ConnectionPool() {
        connections = new ArrayDeque<Connection>(POOL_SIZE);
    }

    public static ConnectionPool getInstance() {
        if(!isInitialized.get()) {
            try{
                LOCK.lock();
                if(!isInitialized.get()) {
                    instance = new ConnectionPool();
                    try {
                        instance.init();
                    } catch (ConnectionException e) {
                        throw new ConnectionPoolException(e.getMessage(), e);
                    }
                }
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }

    private void init() {
        for (int i =0; i < POOL_SIZE; i++) {
            connections.add(connectionFactory.createConnection(PROPERTY_PATH));
        }
        isInitialized.set(true);
    }


    public Connection getConnection() {
        try {
            LOCK.lock();
            semaphore.acquire();
            return connections.poll();
        } catch (InterruptedException e) {
            throw new ConnectionException(e.getMessage(), e);
        } finally {
            LOCK.unlock();
        }
    }

    public boolean releaseConnection(Connection connection) {
        try {
            LOCK.lock();
            boolean result = connections.add(connection);
            if(result) {
                semaphore.release();
            }
            return result;
        } finally {
            LOCK.unlock();
        }
    }

}
