package com.epam.shtoss.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class User implements Serializable, Entity {

    public final static String ID_LABEL = "id";
    public final static String NAME_LABEL = "name";
    public final static String LOGIN_LABEL = "login";
    public final static String SURNAME_LABEL = "surname";
    public final static String PASSWORD_LABEL = "password";
    public final static String IS_ADMIN_LABEL = "is_admin";
    public final static String IS_BLOCKED_LABEL = "is_blocked";
    public final static String MONEY_LABEL = "money";
    public final static String RATING_LABEL = "rating";


    private String name;
    private String surname;
    private String login;
    private String password;
    private Boolean isAdmin;
    private Long id;
    private Boolean isBlocked;
    private BigDecimal money;
    private int rating;

    public User(long id, String name, String surname, String login, String password, Boolean isAdmin, Boolean isBlocked, BigDecimal money, int rating) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.isAdmin = isAdmin;
        this.password = password;
        this.isBlocked = isBlocked;
        this.money = money;
        this.rating = rating;
    }



    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getSurname() {
        return surname;
    }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public Long getId() {
        return id;
    }

    public Boolean isBlocked() {
        return isBlocked;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public int getRating() {
        return rating;
    }
}
