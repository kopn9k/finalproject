package com.epam.shtoss.model.builders;

import com.epam.shtoss.model.Entity;
import com.epam.shtoss.model.Message;
import com.epam.shtoss.model.News;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MessageBuilder implements Builder {

    @Override
    public Entity build(ResultSet resultSet) throws SQLException {
        Long id = (long) resultSet.getInt(Message.ID_LABEL);
        String author = resultSet.getString(Message.AUTHOR_LABEL);
        String recipient = resultSet.getString(Message.RECIPIENT_LABEL);
        String message = resultSet.getString(Message.MESSAGE_LABEL);
        Date date = resultSet.getDate(Message.DATE_LABEL);
        return new Message(id, author, recipient, message, date.toLocalDate());
    }
}
