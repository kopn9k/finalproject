package com.epam.shtoss.model.builders;

import com.epam.shtoss.model.Game;
import com.epam.shtoss.model.News;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GameBuilder implements Builder<Game> {
    @Override
    public Game build(ResultSet resultSet) throws SQLException {
            Long id = (long) resultSet.getInt(Game.ID_LABEL);
            String author = resultSet.getString(Game.USER_LOGIN_LABEL);
            BigDecimal bank = resultSet.getBigDecimal(Game.BANK_LABEL);
            boolean win = resultSet.getBoolean(Game.RESULT_LABEL);
            Date date = resultSet.getDate(Game.DATE_LABEL);
            return new Game(id, author, bank, win, date.toLocalDate());

    }
}
