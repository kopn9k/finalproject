package com.epam.shtoss.model.builders;

import com.epam.shtoss.model.News;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;


public class NewsBuilder implements Builder<News> {

    @Override
    public News build(ResultSet resultSet) throws SQLException {
        Long id = (long) resultSet.getInt(News.ID_LABEL);
        String text = resultSet.getString(News.TEXT_LABEL);
        String author = resultSet.getString(News.AUTHOR_LOGIN_LABEL);
        Date date = resultSet.getDate(News.DATE_LABEL);
        return new News(id, text, date.toLocalDate(), author);

    }
}