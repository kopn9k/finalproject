package com.epam.shtoss.model.builders;

import com.epam.shtoss.model.Game;
import com.epam.shtoss.model.User;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserBuilder implements Builder<User> {


    @Override
    public User build(ResultSet resultSet) throws SQLException {
            Long id = (long) resultSet.getInt(User.ID_LABEL);
            String name = resultSet.getString(User.NAME_LABEL);
            String surname = resultSet.getString(User.SURNAME_LABEL);
            String login = resultSet.getString(User.LOGIN_LABEL);
            String password = resultSet.getString(User.PASSWORD_LABEL);
            Boolean isAdmin = resultSet.getBoolean(User.IS_ADMIN_LABEL);
            Boolean isBlocked = resultSet.getBoolean(User.IS_BLOCKED_LABEL);
            BigDecimal money = resultSet.getBigDecimal(User.MONEY_LABEL);
            int rating = resultSet.getInt(User.RATING_LABEL);
            return new User(id, name, surname, login, password, isAdmin, isBlocked, money, rating);

    }
}
