package com.epam.shtoss.model.builders;

import com.epam.shtoss.model.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Builder<T extends Entity> {
    /**
     *
     * @param resultSet
     * @return Entity
     * @throws SQLException
     */
    T build(ResultSet resultSet) throws SQLException;
}
