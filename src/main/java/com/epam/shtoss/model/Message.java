package com.epam.shtoss.model;

import java.time.LocalDate;

public class Message implements Entity {
    public final static String ID_LABEL = "id";
    public final static String AUTHOR_LABEL = "author";
    public final static String RECIPIENT_LABEL = "recipient";
    public final static String MESSAGE_LABEL = "message";
    public final static String DATE_LABEL = "send_date";


    private Long id;
    private String author;
    private String recipient;
    private String message;
    private LocalDate date;

    public Message(Long id, String author, String recipient, String message, LocalDate date) {
        this.id = id;
        this.author = author;
        this.recipient = recipient;
        this.message = message;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getMessage() {
        return message;
    }

    public LocalDate getDate() {
        return date;
    }
}
