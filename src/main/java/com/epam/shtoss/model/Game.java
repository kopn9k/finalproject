package com.epam.shtoss.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Game implements Entity {

    public final static String ID_LABEL = "id";
    public final static String USER_LOGIN_LABEL = "login";
    public final static String BANK_LABEL = "bank";
    public final static String RESULT_LABEL = "win";
    public final static String DATE_LABEL = "start_date";

    private Long id;
    private String author;
    private BigDecimal bank;
    private boolean win;
    private LocalDate date;

    public Game(Long id, String author, BigDecimal bank, boolean win, LocalDate date) {
        this.id = id;
        this.author = author;
        this.bank = bank;
        this.win = win;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public BigDecimal getBank() {
        return bank;
    }

    public boolean isWin() {
        return win;
    }

    public LocalDate getDate() {
        return date;
    }
}

