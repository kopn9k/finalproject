package com.epam.shtoss.model;

import java.time.LocalDate;

public class News implements Entity {

    public final static String ID_LABEL = "id";
    public final static String TEXT_LABEL = "news_text";
    public final static String DATE_LABEL = "date_of_publishing";
    public final static String AUTHOR_LOGIN_LABEL = "login";


    private Long id;
    private String text;
    private LocalDate date;
    private String author;

    public News(Long id, String text, LocalDate date, String author) {
        this.id = id;
        this.text = text;
        this.date = date;
        this.author = author;
    }

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getAuthor() {
        return author;
    }
}
