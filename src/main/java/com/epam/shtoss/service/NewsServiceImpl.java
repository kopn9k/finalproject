package com.epam.shtoss.service;

import com.epam.shtoss.exceptions.ConnectionException;
import com.epam.shtoss.exceptions.ConnectionPoolException;
import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.News;
import com.epam.shtoss.repository.Repository;
import com.epam.shtoss.repository.RepositoryFactory;
import com.epam.shtoss.specification.AddNewsSpecification;
import com.epam.shtoss.specification.ShowAllNewsSpecification;
import java.util.List;

public class NewsServiceImpl implements Service, NewsService {

    public List<News> showNews() throws RepositoryException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<News> repository = repositoryFactory.getNewsRepository();
            return repository.query(new ShowAllNewsSpecification());
        }
    }

    public boolean insertNews(String text, String author) throws RepositoryException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<News> repository = repositoryFactory.getNewsRepository();
            return repository.add(new AddNewsSpecification(text, author));
        }
    }

}
