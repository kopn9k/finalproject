package com.epam.shtoss.service;

import com.epam.shtoss.exceptions.ConnectionException;
import com.epam.shtoss.exceptions.ConnectionPoolException;
import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.Game;
import com.epam.shtoss.model.News;
import com.epam.shtoss.repository.Repository;
import com.epam.shtoss.repository.RepositoryFactory;
import com.epam.shtoss.specification.AddGameSpecification;
import com.epam.shtoss.specification.AddNewsSpecification;
import com.epam.shtoss.specification.ShowAllGamesSpecification;


import java.math.BigDecimal;
import java.util.List;
//для каждого сервиса сдела интерфейс например геймсервис, а класс уже будет называться гейм сервис импл
public class GameServiceImpl implements Service, GameService {

    public List<Game> showGames() throws RepositoryException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Game> repository = repositoryFactory.getGameRepository();
            return repository.query(new ShowAllGamesSpecification());
        }
    }

    public void addGame(long id, BigDecimal bank, boolean isWin) throws RepositoryException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Game> repository = repositoryFactory.getGameRepository();
            repository.add(new AddGameSpecification(id, bank, isWin));
        }
    }
}
