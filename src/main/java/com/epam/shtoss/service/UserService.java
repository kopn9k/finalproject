package com.epam.shtoss.service;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.User;
import com.epam.shtoss.repository.Repository;
import com.epam.shtoss.repository.RepositoryFactory;
import com.epam.shtoss.specification.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface UserService {
    /**
     * return user by login
     * @param login
     * @param password
     * @return
     * @throws RepositoryException
     */
    public Optional<User> login(String login, String password) throws RepositoryException;

    /**
     * return user by id
     * @param id
     * @return
     * @throws RepositoryException
     */
    public Optional<User> getUser(Long id) throws  RepositoryException;

    /**
     * return all users
     * @return
     * @throws RepositoryException
     */

    public List<User> showUsers() throws  RepositoryException;

    /**
     * update user status
     * @param login
     * @param isBlocked
     * @throws RepositoryException
     */

    public void updateUser(String login, boolean isBlocked) throws  RepositoryException;

    /**
     * addUser into database
     * @param name
     * @param surname
     * @param login
     * @param password
     * @param isAdmin
     * @throws RepositoryException
     */

    public void addUser(String name, String surname, String login, String password,  boolean isAdmin) throws RepositoryException;

    /**
     * update user after a game
     * @param id
     * @param money
     * @param rating
     * @throws RepositoryException
     */

    public void updateAfterGame(Long id, BigDecimal money, int rating) throws RepositoryException;

    /**
     * update user after replenish account
     * @param id
     * @param money
     * @throws RepositoryException
     */
    public void updateAfterReplenish(Long id, BigDecimal money) throws RepositoryException;
}
