package com.epam.shtoss.service;

import com.epam.shtoss.exceptions.ConnectionException;
import com.epam.shtoss.exceptions.ConnectionPoolException;
import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.Game;
import com.epam.shtoss.model.User;
import com.epam.shtoss.repository.Repository;
import com.epam.shtoss.repository.RepositoryFactory;
import com.epam.shtoss.repository.UserRepository;
import com.epam.shtoss.specification.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements Service, UserService {

    /**
     *
     * @param login
     * @param password
     * @return
     * @throws ConnectionException
     * @throws ConnectionPoolException
     * @throws RepositoryException
     */
    public Optional<User> login(String login, String password) throws RepositoryException{
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<User> repository = repositoryFactory.getUserRepository();
            return repository.queryBySingleResult(new UserByLoginAndPasswordSpecification(login,password));
        }
    }

    public Optional<User> getUser(Long id) throws  RepositoryException{
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<User> repository = repositoryFactory.getUserRepository();
            return repository.queryBySingleResult(new UserByIdSpecification(id));
        }
    }

    public List<User> showUsers() throws  RepositoryException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<User> repository = repositoryFactory.getUserRepository();
            return repository.query(new ShowAllUsersSpecification());
        }
    }

    public void updateUser(String login, boolean isBlocked) throws  RepositoryException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<User> repository = repositoryFactory.getUserRepository();
            repository.add(new UpdateUserByLoginSpecification(login, isBlocked));
        }
    }

    public void addUser(String name, String surname, String login, String password,  boolean isAdmin) throws RepositoryException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<User> repository = repositoryFactory.getUserRepository();
            repository.add(new AddUserSpecification(name, surname, login, isAdmin, password));
        }
    }

    public void updateAfterGame(Long id, BigDecimal money, int rating) throws RepositoryException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<User> repository = repositoryFactory.getUserRepository();
            repository.add(new UpdateUserAfterGameSpecification(id, money, rating));
        }
    }

    public void updateAfterReplenish(Long id, BigDecimal money) throws RepositoryException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<User> repository = repositoryFactory.getUserRepository();
            repository.add(new UpdateUserAfterReplenishSpecification(id, money));
        }
    }


}
