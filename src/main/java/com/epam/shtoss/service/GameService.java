package com.epam.shtoss.service;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.Game;

import java.math.BigDecimal;
import java.util.List;

public interface GameService {
    /**
     * show all games
     * @return
     * @throws RepositoryException
     */
    public List<Game> showGames() throws RepositoryException;

    /**
     * add game into database
     * @param id
     * @param bank
     * @param isWin
     * @throws RepositoryException
     */
    public void addGame(long id, BigDecimal bank, boolean isWin) throws RepositoryException;
}
