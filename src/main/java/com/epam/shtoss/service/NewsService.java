package com.epam.shtoss.service;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.News;
import com.epam.shtoss.repository.Repository;
import com.epam.shtoss.repository.RepositoryFactory;
import com.epam.shtoss.specification.AddNewsSpecification;
import com.epam.shtoss.specification.ShowAllNewsSpecification;

import java.util.List;

public interface NewsService {
    /**
     * show all news
     * @return
     * @throws RepositoryException
     */
    public List<News> showNews() throws RepositoryException;

    /**
     * insert news
     * @param text
     * @param author
     * @return
     * @throws RepositoryException
     */

    public boolean insertNews(String text, String author) throws RepositoryException;
}
