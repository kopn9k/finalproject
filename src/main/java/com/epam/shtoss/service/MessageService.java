package com.epam.shtoss.service;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.Message;


import java.util.List;

public interface MessageService {
    /**
     * show all incoming messages for this recipient
     * @param recipient
     * @return
     * @throws RepositoryException
     */
    public List<Message> showIncomingMessages(String recipient) throws RepositoryException;

    /**
     * show all sent messages write by this author
     * @param author
     * @return
     * @throws RepositoryException
     */
    public List<Message> showSentMessages(String author) throws RepositoryException;

    /**
     * insert message into database
     * @param message
     * @param recipient
     * @param id
     * @throws RepositoryException
     */

    public void insertMessage(String message, String recipient, Long id) throws RepositoryException;
}
