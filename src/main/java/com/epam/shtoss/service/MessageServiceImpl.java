package com.epam.shtoss.service;

import com.epam.shtoss.command.ShowIncomingMessages;
import com.epam.shtoss.exceptions.ConnectionException;
import com.epam.shtoss.exceptions.ConnectionPoolException;
import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.Message;
import com.epam.shtoss.model.News;
import com.epam.shtoss.repository.Repository;
import com.epam.shtoss.repository.RepositoryFactory;
import com.epam.shtoss.specification.InsertMessageSpecification;
import com.epam.shtoss.specification.ShowAllNewsSpecification;
import com.epam.shtoss.specification.ShowIncomingMessagesSpecification;
import com.epam.shtoss.specification.ShowSentMessagesSpecification;

import java.util.List;

public class MessageServiceImpl implements Service, MessageService {

    public List<Message> showIncomingMessages(String recipient) throws RepositoryException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Message> repository = repositoryFactory.getMessageRepository();
            return repository.query(new ShowIncomingMessagesSpecification(recipient));
        }
    }

    public List<Message> showSentMessages(String author) throws RepositoryException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Message> repository = repositoryFactory.getMessageRepository();
            return repository.query(new ShowSentMessagesSpecification(author));
        }
    }

    public void insertMessage(String message, String recipient, Long id) throws RepositoryException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Message> repository = repositoryFactory.getMessageRepository();
            repository.add(new InsertMessageSpecification(message, recipient, id));
        }
    }
}
