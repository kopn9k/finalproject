package com.epam.shtoss.controller;

import com.epam.shtoss.command.AbstractCommand;
import com.epam.shtoss.command.Command;
import com.epam.shtoss.command.CommandFactory;
import com.epam.shtoss.command.CommandResult;
import com.epam.shtoss.exceptions.ConnectionException;
import com.epam.shtoss.exceptions.ConnectionPoolException;
import com.epam.shtoss.exceptions.RepositoryException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class Controller extends HttpServlet {

    private final static String COMMAND ="command";
        private static Logger logger = LogManager.getLogger();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response, "get");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response, "post");
    }

    private void process(HttpServletRequest request, HttpServletResponse response, String httpMethod) throws ServletException, IOException {
        String command = request.getParameter(COMMAND);
        AbstractCommand action = CommandFactory.create(command);
        if(action.getMethodType().equals(httpMethod)) {
            try {
                CommandResult commandResult = action.execute(request, response);
                String page = commandResult.getPage();
                if (commandResult.isRedirect()) {
                    response.sendRedirect(page);
                } else {
                    request.getRequestDispatcher(page).forward(request, response);
                }
            } catch (Exception e) {
                request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request, response);
                logger.error(e.getMessage(), e);
            }
        } else {
            request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request, response);
        }


    }

}
