package com.epam.shtoss.command;


public abstract class AbstractCommand implements Command {

    /*private final String methodType;

    private AbstractCommand(String methodType) {
        this.methodType = methodType;
    }

    public static AbstractCommand post(String methodType) {
        return new Command(methodType);
    }
    public static AbstractCommand get(String methodType) {
        return new CommandResult(page, true);
    }*/

    private final String methodType;

    public AbstractCommand(String methodType) {
        this.methodType = methodType;
    }

    public String getMethodType() {
        return methodType;
    }
}
