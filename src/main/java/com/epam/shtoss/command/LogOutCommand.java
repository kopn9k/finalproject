package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.RepositoryException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogOutCommand extends AbstractCommand {
    public LogOutCommand(String methodType) {
        super(methodType);
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws RepositoryException {
            HttpSession session = request.getSession(true);
            session.setAttribute("user", null);
            return CommandResult.forward("/WEB-INF/pages/login.jsp");
    }
}
