package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.Message;
import com.epam.shtoss.model.User;
import com.epam.shtoss.service.MessageServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ShowSentMessages extends AbstractCommand {
    private final String PAGE = "page";

    public ShowSentMessages(String methodType) {
        super(methodType);
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws RepositoryException {
        MessageServiceImpl service = new MessageServiceImpl();
        String pageString = request.getParameter(PAGE);
        int page;
        if(pageString == null) {
            page =  1;
        } else {
            page = Integer.parseInt(pageString);
        }
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        String author = user.getLogin();
        List<Message> messages = service.showSentMessages(author);
        request.setAttribute("page", page);
        request.setAttribute("messages", messages);
        return CommandResult.forward("/WEB-INF/pages/sent_messages.jsp");
    }
}
