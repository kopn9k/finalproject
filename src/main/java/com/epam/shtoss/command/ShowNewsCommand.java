package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.News;
import com.epam.shtoss.service.NewsServiceImpl;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class ShowNewsCommand extends AbstractCommand{
    private final String PAGE = "page";

    public ShowNewsCommand(String methodType) {
        super(methodType);
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws RepositoryException {
        NewsServiceImpl service = new NewsServiceImpl();
        String pageString = request.getParameter(PAGE);
        int page;
        if(pageString == null) {
            page =  1;
        } else {
            page = Integer.parseInt(pageString);
        }
        List<News> news = service.showNews();
        request.setAttribute("page", page);
        request.setAttribute("news", news);
        return CommandResult.forward("/WEB-INF/pages/news.jsp");
    }
}
