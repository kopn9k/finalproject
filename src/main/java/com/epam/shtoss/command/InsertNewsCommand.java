package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.service.NewsServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class InsertNewsCommand extends AbstractCommand {
    public final static String TEXT = "text";
    public final static String AUTHOR = "author";

    public InsertNewsCommand(String methodType) {
        super(methodType);
    }


    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws RepositoryException {
        NewsServiceImpl service = new NewsServiceImpl();
        String text = request.getParameter(TEXT);
        String author = request.getParameter(AUTHOR);
        boolean result = service.insertNews(text, author);
        if(result) {
            return CommandResult.redirect("controller?command=goSuccessfulInsert");

        }
        return CommandResult.redirect("controller?command=goUnsuccessfulInsert");
    }
}
