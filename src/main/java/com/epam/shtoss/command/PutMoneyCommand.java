package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.User;
import com.epam.shtoss.service.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Optional;

public class PutMoneyCommand extends AbstractCommand {
    private final static String MONEY = "summ";

    public PutMoneyCommand(String methodType) {
        super(methodType);
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws RepositoryException {
        UserServiceImpl service = new UserServiceImpl();
        String moneyString = request.getParameter(MONEY);
        Double moneyDouble = Double.parseDouble(moneyString);
        BigDecimal summ = new BigDecimal(moneyDouble);
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        Long id = user.getId();
        BigDecimal userMoney = user.getMoney();
        BigDecimal updateMoney = userMoney.add(summ);
        service.updateAfterReplenish(id, updateMoney);
        Optional<User> newUser = service.getUser(id);
        if (newUser.isPresent()) {
            session = request.getSession(true);
            session.setAttribute("user", newUser.get());
            return CommandResult.redirect("controller?command=successfulPut");
        }
        return CommandResult.redirect("controller?command=goErrorPage");


    }
}
