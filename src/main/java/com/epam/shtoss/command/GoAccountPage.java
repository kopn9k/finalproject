package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.RepositoryException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GoAccountPage extends AbstractCommand {
    public GoAccountPage(String methodType) {
        super(methodType);
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws RepositoryException {
        return CommandResult.forward("/WEB-INF/pages/account.jsp");
    }
}
