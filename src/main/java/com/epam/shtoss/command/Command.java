package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.ConnectionException;
import com.epam.shtoss.exceptions.ConnectionPoolException;
import com.epam.shtoss.exceptions.RepositoryException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 */
public interface Command {

    /**
     *
     * @param request
     * @param response
     * @return return result of command execution(page, and redirect type)
     * @throws ConnectionException
     * @throws ConnectionPoolException
     * @throws RepositoryException
     */

    CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws RepositoryException;
}
