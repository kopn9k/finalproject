package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.service.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddUserCommand extends AbstractCommand {
    public final static String NAME = "name";
    public final static String SURNAME = "surname";
    public final static String LOGIN = "login";
    public final static String PASSWORD = "password";
    public final static String IS_ADMIN = "isAdmin";

    public AddUserCommand(String methodType) {
        super(methodType);
    }


    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws RepositoryException {
        UserServiceImpl service = new UserServiceImpl();
        String name = request.getParameter(NAME);
        String surname = request.getParameter(SURNAME);
        String login = request.getParameter(LOGIN);
        String password = request.getParameter(PASSWORD);
        String adminStatus = request.getParameter(IS_ADMIN);
        boolean isAdmin = false;
        if(adminStatus.equals("true")) {
            isAdmin = true;
        }
        service.addUser(name, surname, login, password, isAdmin);
        return CommandResult.redirect("controller?command=goSuccessfulAdd");

    }


}
