package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.Game;
import com.epam.shtoss.service.GameServiceImpl;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GoGamePage extends AbstractCommand {
    private final String PAGE = "page";

    public GoGamePage(String methodType) {
        super(methodType);
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws RepositoryException {
        GameServiceImpl service = new GameServiceImpl();
        String pageString = request.getParameter(PAGE);
        int page;
        if(pageString == null) {
            page =  1;
        } else {
            page = Integer.parseInt(pageString);
        }
        List<Game> games = service.showGames();
        request.setAttribute("page", page);
        request.setAttribute("gamesForShow", games);
        return CommandResult.forward("/WEB-INF/pages/game.jsp");
    }
}
