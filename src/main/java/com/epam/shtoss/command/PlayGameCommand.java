package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.logic.GameSimulator;
import com.epam.shtoss.logic.ValueGenerator;
import com.epam.shtoss.model.User;
import com.epam.shtoss.service.GameServiceImpl;
import com.epam.shtoss.service.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Optional;

public class PlayGameCommand extends AbstractCommand {
    public final static String BANK = "bank";
    public final static String CARD = "card";
    public final static int VALUE_GENERATOR =  (int)(Math.random()*2);
    private  GameSimulator gameSimulator;

    public PlayGameCommand(String methodType) {
        super(methodType);
        this.gameSimulator = new GameSimulator(new ValueGenerator(VALUE_GENERATOR));
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws RepositoryException {
        GameServiceImpl gameServiceImpl = new GameServiceImpl();
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        Long userId = user.getId();
        String bank = request.getParameter(BANK);
        Double bankDouble = Double.parseDouble(bank);
        BigDecimal bankDecimal = new BigDecimal(bankDouble);
        String card = request.getParameter(CARD);
        if(!gameSimulator.isCard(card)) {
            return CommandResult.redirect("controller?command=goErrorPage");
        }
        boolean result = gameSimulator.playAGame(card);
        gameServiceImpl.addGame(userId, bankDecimal, result);
        UserServiceImpl userServiceImpl = new UserServiceImpl();
        BigDecimal money = user.getMoney();
        int rating = user.getRating();
        if(result) {
            money = money.add(bankDecimal);
            rating +=20;
        } else {
            money = money.subtract(bankDecimal);
            rating -=20;
        }
        userServiceImpl.updateAfterGame(userId, money, rating);
        Optional<User> newUser = userServiceImpl.getUser(userId);
        if (newUser.isPresent()) {
            session = request.getSession(true);
            session.setAttribute("user", newUser.get());
            if(result) {
                return CommandResult.redirect("controller?command=goWinPage");

            }
            return CommandResult.redirect("controller?command=goLosePage");
        }
        return CommandResult.redirect("controller?command=goErrorPage");

    }
}
