package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.service.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BlockUserCommand extends AbstractCommand {
    public final static String LOGIN = "login";
    public final static String BLOCK = "block";

    public BlockUserCommand(String methodType) {
        super(methodType);
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws RepositoryException {
        UserServiceImpl service = new UserServiceImpl();
        String login = request.getParameter(LOGIN);
        String status = request.getParameter(BLOCK);
        boolean isBlocked = false;
        if(status.equals("true")) {
            isBlocked = true;
        }
        service.updateUser(login, isBlocked);
        return CommandResult.redirect("controller?command=goSuccessfulBlock");
    }
}
