package com.epam.shtoss.command;

public class CommandFactory {

    private final static String POST="post";
    private final static String GET="get";

    public static AbstractCommand create(String command) {

        switch (command) {
            case ("login"): {
                return new LoginCommand(POST);
            }
            case ("showNews"): {
                return new ShowNewsCommand(GET);
            }
            case ("insertNews"): {
                return new InsertNewsCommand(POST);
            }
            case ("goGamePage"): {
                return new GoGamePage(GET);
            }
            case ("playGame"): {
                return new PlayGameCommand(POST);
            }
            case ("switchLanguage"): {
                return new SwitchLanguageCommand(GET);
            }
            case ("goHome"): {
                return new GoHomeCommand(GET);
            }
            case ("goPlayersPage"): {
                return new GoPlayersPage(GET);
            }
            case ("blockUser"): {
                return new BlockUserCommand(POST);
            }
            case ("addUser"): {
                return new AddUserCommand(POST);
            }
            case ("goLoginPage"): {
                return new GoLoginPage(GET);
            }
            case ("logOut"): {
                return new LogOutCommand(GET);
            }
            case ("goAccountPage"): {
                return new GoAccountPage(GET);
            }
            case ("putMoney"): {
                return new PutMoneyCommand(POST);
            }
            case ("showIncomingMessages"): {
                return new ShowIncomingMessages(GET);
            }
            case ("showSentMessages"): {
                return new ShowSentMessages(GET);
            }
            case ("sendMessage"): {
                return new SendMessageCommand(POST);
            }
            case ("goSendMessagePage"): {
                return new GoSendMessagePage(GET);
            }
            case ("goWinPage"): {
                return new GoWinPage(GET);
            }
            case ("goLosePage"): {
                return new GoLosePage(GET);
            }
            case ("goErrorPage"): {
                return new GoErrorPage(GET);
            }
            case ("successfulSend"): {
                return new GoSuccessfulSendPage(GET);
            }
            case ("successfulPut"): {
                return new GoSuccessfulPutPage(GET);
            }
            case ("goSuccessfulInsert"): {
                return new GoSuccessfulInsertPage(GET);
            }
            case ("goUnsuccessfulInsert"): {
                return new GoUnsuccessfulInsert(GET);
            }
            case ("goSuccessfulAdd"): {
                return new GoSuccessfulAddPage(GET);
            }
            case ("goSuccessfulBlock"): {
                return new GoSuccessfulBlockPage(GET);
            }
            case ("goAbandonUser"): {
                return new GoAbandonUserPage(GET);
            }
            default: {
                throw new IllegalArgumentException();
            }
        }

    }
}
