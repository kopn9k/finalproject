package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.User;
import com.epam.shtoss.service.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GoPlayersPage extends AbstractCommand {
    public GoPlayersPage(String methodType) {
        super(methodType);
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws RepositoryException {
        UserServiceImpl service = new UserServiceImpl();
        List<User> users = service.showUsers();
        request.setAttribute("users", users);
        return CommandResult.forward("/WEB-INF/pages/players.jsp");
    }
}
