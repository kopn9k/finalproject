package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.ConnectionException;
import com.epam.shtoss.exceptions.ConnectionPoolException;
import com.epam.shtoss.exceptions.RepositoryException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GoSuccessfulBlockPage extends AbstractCommand {
    public GoSuccessfulBlockPage(String methodType) {
        super(methodType);
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws RepositoryException {
        return CommandResult.forward("/WEB-INF/pages/successful_block.jsp");

    }
}
