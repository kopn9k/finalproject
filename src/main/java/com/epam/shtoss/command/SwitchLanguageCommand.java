package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.RepositoryException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SwitchLanguageCommand extends AbstractCommand {
    private final String LANGUAGE = "language";

    public SwitchLanguageCommand(String methodType) {
        super(methodType);
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws RepositoryException {
        String language = request.getParameter(LANGUAGE);
        HttpSession session = request.getSession(true);
        session.setAttribute("language", language);
        return CommandResult.forward("/WEB-INF/pages/main.jsp");
    }
}
