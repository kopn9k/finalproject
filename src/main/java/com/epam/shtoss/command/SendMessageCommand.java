package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.User;
import com.epam.shtoss.service.MessageService;
import com.epam.shtoss.service.MessageServiceImpl;
import com.epam.shtoss.service.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SendMessageCommand extends AbstractCommand {
    public final static String MESSAGE = "message";
    public final static String RECIPIENT = "recipient";

    public SendMessageCommand(String methodType) {
        super(methodType);
    }


    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws RepositoryException {
        MessageService service = new MessageServiceImpl();
        String message = request.getParameter(MESSAGE);
        String recipient = request.getParameter(RECIPIENT);
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        Long authorId = user.getId();
        service.insertMessage(message, recipient, authorId);
        return CommandResult.redirect("controller?command=successfulSend");
    }
}
