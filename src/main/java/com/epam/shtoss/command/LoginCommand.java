package com.epam.shtoss.command;

import com.epam.shtoss.exceptions.RepositoryException;
import com.epam.shtoss.model.User;
import com.epam.shtoss.service.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

public class LoginCommand extends AbstractCommand {
    public final static String LOGIN = "login";
    public final static String PASSWORD = "password";

    public LoginCommand(String methodType) {
        super(methodType);
    }


    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws  RepositoryException {
        UserServiceImpl service = new UserServiceImpl();
        String login = request.getParameter(LOGIN);
        String password = request.getParameter(PASSWORD);
        Optional<User> user = service.login(login, password);
        if (user.isPresent()) {
            if(user.get().isBlocked()) {
                return CommandResult.redirect("controller?command=goAbandonUser");
            }
            HttpSession session = request.getSession(true);
            session.setAttribute("user", user.get());
            return CommandResult.redirect("controller?command=goHome");
        }
        return CommandResult.redirect("controller?command=goErrorPage");

    }
}
