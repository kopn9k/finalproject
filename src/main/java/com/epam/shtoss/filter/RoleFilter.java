package com.epam.shtoss.filter;

import com.epam.shtoss.model.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * provide access to pages according user role
 */

public class RoleFilter implements Filter {
    private Map<UserRoles, List<String>> rolesMap;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        rolesMap = new HashMap<>();
        rolesMap.put(UserRoles.USER, getUserList());
        rolesMap.put(UserRoles.ADMIN, getAdminList());
        rolesMap.put(UserRoles.UNAUTHORIZED, getUnauthorizedList());

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest)servletRequest;
        UserRoles userRole = getUserRoleByRequest(httpServletRequest);

        String command = servletRequest.getParameter("command");
        List<String> commands = rolesMap.get(userRole);

        if (commands.contains(command)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            RequestDispatcher requestDispatcher = servletRequest.getRequestDispatcher("/WEB-INF/pages/denied_access.jsp");
            requestDispatcher.forward(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }

    private UserRoles getUserRoleByRequest(HttpServletRequest httpServletRequest) {
        HttpSession session = httpServletRequest.getSession(false);
        if (session == null) {
            return UserRoles.UNAUTHORIZED;
        } else {
            User user = (User)session.getAttribute("user");
            if (user == null) {
                return UserRoles.UNAUTHORIZED;
            }
            boolean isAdmin = user.getIsAdmin();
            if (isAdmin) {
                return UserRoles.ADMIN;
            } else {
                return UserRoles.USER;
            }
        }
    }

    private List<String> getUserList() {
        List<String> result = new ArrayList<>();
        result.add("login");
        result.add("showNews");
        result.add("goGamePage");
        result.add("playGame");
        result.add("goHome");
        result.add("goLoginPage");
        result.add("logOut");
        result.add("goAccountPage");
        result.add("putMoney");
        result.add("goSendMessagePage");
        result.add("showIncomingMessages");
        result.add("showSentMessages");
        result.add("sendMessage");
        result.add("goWinPage");
        result.add("goLosePage");
        result.add("goErrorPage");
        result.add("successfulSend");
        result.add("successfulPut");
        result.add("goAbandonUser");
        result.add("switchLanguage");
        return result;
    }

    private List<String> getAdminList() {
        List<String> result = new ArrayList<>();
        result.add("login");
        result.add("showNews");
        result.add("goGamePage");
        result.add("goHome");
        result.add("goLoginPage");
        result.add("logOut");
        result.add("showIncomingMessages");
        result.add("showSentMessages");
        result.add("sendMessage");
        result.add("goSendMessagePage");
        result.add("insertNews");
        result.add("goPlayersPage");
        result.add("blockUser");
        result.add("addUser");
        result.add("goPlayersPage");
        result.add("goErrorPage");
        result.add("successfulSend");
        result.add("goSuccessfulInsert");
        result.add("goUnsuccessfulInsert");
        result.add("goSuccessfulAdd");
        result.add("goSuccessfulBlock");
        result.add("goAbandonUser");
        result.add("switchLanguage");
        return result;
    }

    private List<String> getUnauthorizedList() {
        List<String> result = new ArrayList<>();
        result.add("login");
        result.add("goLoginPage");
        return result;
    }

}
