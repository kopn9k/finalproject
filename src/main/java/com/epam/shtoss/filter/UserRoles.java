package com.epam.shtoss.filter;

public enum UserRoles {
    ADMIN("ADMIN"),
    USER("USER"),
    UNAUTHORIZED("UNAUTHORIZED");

    private String value;

    private UserRoles(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
