package com.epam.shtoss.specification;

import java.util.ArrayList;
import java.util.List;

public class ShowAllUsersSpecification implements SqlSpecification {
    @Override
    public String toSqlClauses() {
        return "select * from Shtoss.users";

    }

    @Override
    public List<Object> getParameters() {
        List<Object> result = new ArrayList<>();
        result.add("showAllUsers");
        return result;
    }

}
