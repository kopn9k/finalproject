package com.epam.shtoss.specification;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AddUserSpecification implements SqlSpecification {

    private String name;
    private String surname;
    private String login;
    private boolean isAdmin;
    private String password;
    private boolean isBlocked;
    private BigDecimal money;
    private int rating;

    public AddUserSpecification(String name, String surname, String login, boolean isAdmin, String password) {
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.isAdmin = isAdmin;
        this.password = password;
        this.isBlocked = false;
        this.money = new BigDecimal(1000);
        this.rating = 1000;
    }

    @Override
    public String toSqlClauses() {
        return "insert into Shtoss.users ( name, surname, login, is_admin, password, is_blocked, money, rating) values (?, ?, ?, ?, ?, ?, ?, ?)";
    }

    @Override
    public List<Object> getParameters() {
        List<Object> result = new ArrayList<>();
        result.add("add");
        result.add(name);
        result.add(surname);
        result.add(login);
        result.add(isAdmin);
        result.add(password);
        result.add(isBlocked);
        result.add(money);
        result.add(rating);
        return result;
    }

}
