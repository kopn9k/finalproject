package com.epam.shtoss.specification;

import java.util.ArrayList;
import java.util.List;

public class ShowIncomingMessagesSpecification implements SqlSpecification {
    private String login;

    public ShowIncomingMessagesSpecification(String login) {
        this.login = login;
    }

    @Override
    public String toSqlClauses() {
        return "select messages.id, u1.login as author, u2.login as recipient, messages.message, messages.send_date  from shtoss.messages inner join shtoss.users u1 on u1.id = shtoss.messages.user_id_from inner join shtoss.users u2 on u2.id = shtoss.messages.user_id_to where u2.login = ? order by send_date desc, id desc";
    }

    @Override
    public List<Object> getParameters() {
        List<Object> result = new ArrayList<>();
        result.add("showMessages");
        result.add(login);
        return result;
    }

}
