package com.epam.shtoss.specification;

import java.util.ArrayList;
import java.util.List;

public class    ShowAllNewsSpecification implements SqlSpecification {
    @Override
    public String toSqlClauses() {
        return "select news.id, news.news_text, users.login, news.date_of_publishing from shtoss.news inner join shtoss.users on shtoss.users.id = shtoss.news.author_id order by date_of_publishing desc, id desc";
    }

    @Override
    public List<Object> getParameters() {
        List<Object> result = new ArrayList<>();
        result.add("showAllNews");
        return result;
    }

}
