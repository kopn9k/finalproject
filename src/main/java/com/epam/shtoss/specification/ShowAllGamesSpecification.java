package com.epam.shtoss.specification;

import java.util.ArrayList;
import java.util.List;

public class ShowAllGamesSpecification implements SqlSpecification {
    @Override
    public String toSqlClauses() {
        return "select games.id, users.login, games.bank, games.win, games.start_date from shtoss.games inner join shtoss.users on shtoss.users.id = shtoss.games.user_id order by start_date desc, id desc";
    }

    @Override
    public List<Object> getParameters() {
        List<Object> result = new ArrayList<>();
        result.add("showAllGames");
        return result;
    }

}
