package com.epam.shtoss.specification;

import java.util.ArrayList;
import java.util.List;

public class UserByIdSpecification implements SqlSpecification {
    private Long id;

    public UserByIdSpecification(Long id) {
        this.id = id;
    }

    @Override
    public String toSqlClauses() {
        return "select * from shtoss.users where users.id = ?";
    }

    @Override
    public List<Object> getParameters() {
        List<Object> result = new ArrayList<>();
        result.add("getUser");
        result.add(id);
        return result;
    }

}
