package com.epam.shtoss.specification;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class InsertMessageSpecification implements SqlSpecification {

    private Long author;
    private String message;
    private String date;
    private String recipient;

    public InsertMessageSpecification(String message, String recipient, Long author) {
        this.author = author;
        this.message = message;
        this.recipient = recipient;
        LocalDate localDate = LocalDate.now();
        this.date = String.valueOf(localDate.getYear()) +
                "." +
                localDate.getMonthValue() +
                "." +
                localDate.getDayOfMonth();
    }

    @Override
    public String toSqlClauses() {
        return "INSERT INTO shtoss.messages (user_id_from, message, send_date, user_id_to) SELECT ?, ?, ?, id FROM users WHERE login = ?";
    }

    @Override
    public List<Object> getParameters() {
        List<Object> result = new ArrayList<>();
        result.add("insertMessage");
        result.add(author);
        result.add(message);
        result.add(date);
        result.add(recipient);
        return result;
    }

}
