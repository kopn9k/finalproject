package com.epam.shtoss.specification;


import com.epam.shtoss.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserByLoginAndPasswordSpecification implements SqlSpecification {

    private String login;
    private String password;

    public UserByLoginAndPasswordSpecification(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @Override
    public String toSqlClauses() {
        return "select * from Shtoss.users where login = ? and `password` = ?";
    }

    @Override
    public List<Object> getParameters() {
        List<Object> result = new ArrayList<>();
        result.add("login");
        result.add(login);
        result.add(password);
        return result;
    }

}
