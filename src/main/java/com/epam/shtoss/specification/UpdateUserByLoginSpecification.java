package com.epam.shtoss.specification;

import java.util.ArrayList;
import java.util.List;

public class UpdateUserByLoginSpecification implements SqlSpecification {
    private String login;
    private boolean isBlocked;

    public UpdateUserByLoginSpecification(String login, boolean isBlocked) {
        this.login = login;
        this.isBlocked = isBlocked;
    }

    @Override
    public String toSqlClauses() {
        return "update shtoss.users set users.is_blocked = ? where users.login = ?";
    }

    @Override
    public List<Object> getParameters() {
        List<Object> result = new ArrayList<>();
        result.add("update");
        result.add(isBlocked);
        result.add(login);
        return result;
    }

}
