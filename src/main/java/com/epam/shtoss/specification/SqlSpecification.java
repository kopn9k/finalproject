package com.epam.shtoss.specification;

import com.epam.shtoss.model.Entity;

import java.util.List;

public interface SqlSpecification {
    /**
     *
     * @return sql representation of specification
     */
    String toSqlClauses();

    /**
     *
     * @return list of parameters
     */
    List<Object> getParameters();

}
