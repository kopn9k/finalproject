package com.epam.shtoss.specification;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class UpdateUserAfterReplenishSpecification implements SqlSpecification {
    private long id;
    private BigDecimal money;

    public UpdateUserAfterReplenishSpecification(long id, BigDecimal money) {
        this.id = id;
        this.money = money;
    }
    @Override
    public String toSqlClauses() {
        return "update shtoss.users set users.money = ? where users.id = ?";
    }

    @Override
    public List<Object> getParameters() {
        List<Object> result = new ArrayList<>();
        result.add("updateAfterReplenish");
        result.add(money);
        result.add(id);
        return result;
    }

}
