package com.epam.shtoss.specification;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AddNewsSpecification implements SqlSpecification {

    private String text;
    private String author;
    private String date;

    public AddNewsSpecification(String text, String author) {
        this.text = text;
        this.author = author;
        LocalDate localDate = LocalDate.now();
        this.date = String.valueOf(localDate.getYear()) +
                "." +
                localDate.getMonthValue() +
                "." +
                localDate.getDayOfMonth();

    }

    @Override
    public String toSqlClauses() {
        return "INSERT INTO shtoss.news (news_text, date_of_publishing, author_id) " +
                "SELECT ? , ?, id FROM users WHERE login = ?";
    }

    @Override
    public List<Object> getParameters() {
        List<Object> result = new ArrayList<>();
        result.add("insert");
        result.add(text);
        result.add(date);
        result.add(author);
        return result;
    }

}
