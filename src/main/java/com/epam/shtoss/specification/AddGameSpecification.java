package com.epam.shtoss.specification;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AddGameSpecification implements SqlSpecification {

    private long id;
    private BigDecimal bank;
    private boolean isWin;
    private String date;

    public AddGameSpecification(long id, BigDecimal bank, boolean isWin) {
        this.id = id;
        this.bank = bank;
        this.isWin = isWin;
        LocalDate localDate = LocalDate.now();
        this.date = String.valueOf(localDate.getYear()) +
                "." +
                localDate.getMonthValue() +
                "." +
                localDate.getDayOfMonth();

    }

    @Override
    public String toSqlClauses() {
        return "insert into Shtoss.games (user_id, bank, win, start_date)" +
                "values (?, ?, ?, ?)";
    }

    @Override
    public List<Object> getParameters() {
        List<Object> result = new ArrayList<>();
        result.add("add");
        result.add(id);
        result.add(bank);
        result.add(isWin);
        result.add(date);
        return result;
    }

}
