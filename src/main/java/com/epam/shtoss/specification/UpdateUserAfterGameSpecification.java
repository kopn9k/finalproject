package com.epam.shtoss.specification;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class UpdateUserAfterGameSpecification implements SqlSpecification {
    private long id;
    private BigDecimal money;
    private int rating;

    public UpdateUserAfterGameSpecification(long id, BigDecimal money, int rating) {
        this.id = id;
        this.money = money;
        this.rating = rating;
    }

    @Override
    public String toSqlClauses() {
        return "update shtoss.users set users.money = ?, users.rating = ? where users.id = ?";
    }

    @Override
    public List<Object> getParameters() {
        List<Object> result = new ArrayList<>();
        result.add("updateAfterGame");
        result.add(money);
        result.add(rating);
        result.add(id);
        return result;
    }

}
