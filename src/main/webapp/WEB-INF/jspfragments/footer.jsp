  <div id="sections">
    <h2><fmt:message key="navigation" bundle="${ rb }" /></h2>
    <ul>
      <li><a href="${pageContext.servletContext.contextPath}/controller?command=goHome"><fmt:message key="home" bundle="${ rb }" /></a>
      <li><a href="${pageContext.servletContext.contextPath}/controller?command=goGamePage"><fmt:message key="games" bundle="${ rb }" /></a>
      <li><a href="${pageContext.servletContext.contextPath}/controller?command=showNews"><fmt:message key="news" bundle="${ rb }" /></a>
      <li><a href="${pageContext.servletContext.contextPath}/controller?command=showIncomingMessages"><fmt:message key="incoming_messages" bundle="${ rb }" /></a>
      <li><a href="${pageContext.servletContext.contextPath}/controller?command=showSentMessages"><fmt:message key="sent_messages" bundle="${ rb }" /></a>
      <li><a href="${pageContext.servletContext.contextPath}/controller?command=goSendMessagePage"><fmt:message key="send_message" bundle="${ rb }" /></a>
      <c:if test="${sessionScope.user.isAdmin eq 'false'}">
      <li><a href="${pageContext.servletContext.contextPath}/controller?command=goAccountPage"><fmt:message key="account" bundle="${ rb }" /></a>
      </c:if>
      <c:if test="${sessionScope.user.isAdmin eq 'true'}">
      <li><a href="${pageContext.servletContext.contextPath}/controller?command=goPlayersPage"><fmt:message key="players" bundle="${ rb }" /></a>
      </c:if>
    </ul>
    <div id="search"><fmt:message key="language" bundle="${ rb }" />:
					<a href="${pageContext.servletContext.contextPath}/controller?command=switchLanguage&language=ru_RU">ru</a>
					<a href="${pageContext.servletContext.contextPath}/controller?command=switchLanguage&language=en_EN">en</a>
					<p><fmt:message key="money" bundle="${ rb }" /> ${sessionScope.user.getMoney()}</p>
					<p><fmt:message key="rating" bundle="${ rb }" />: ${sessionScope.user.getRating()}</p>
					<a href="${pageContext.servletContext.contextPath}/controller?command=logOut"><fmt:message key="log_out" bundle="${ rb }" /></a>
	</div>

  </div>


</div>

<div id="meta">
  <p><fmt:message key="producer" bundle="${ rb }" />, &copy; 2018
  <p><fmt:message key="footer" bundle="${ rb }" /> +375445754172
</div>
</body>
</html>