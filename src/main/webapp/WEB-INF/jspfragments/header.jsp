<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" language ="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List,com.epam.shtoss.model.Game,com.epam.shtoss.model.User,com.epam.shtoss.model.News,com.epam.shtoss.model.Message" %>
<%@ taglib prefix="ctg" uri="customtags"%>
<c:choose>
<c:when test="${sessionScope.language == null}">
<fmt:setLocale value="en_EN" scope="session" />
</c:when>
<c:otherwise>
<fmt:setLocale value="${sessionScope.language}" scope="session" />
</c:otherwise>
</c:choose>
<fmt:setBundle basename="Bundle" var="rb" />
<!DOCTYPE html>
<html>
	<head>
		<meta charset=“UTF-8”>
		<title><fmt:message key="shtoss" bundle="${ rb }" /></title>
		<link rel="stylesheet" href="styles/style.css">
	</head>

<body>
<div id="title">
  <h1><fmt:message key="shtoss" bundle="${ rb }" /></h1>
</div>

<div id="content">

