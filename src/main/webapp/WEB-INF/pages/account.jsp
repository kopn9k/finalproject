<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" language ="java"%>
<%@ include file="../jspfragments/header.jsp" %>
  <div id="main">
<form action="${pageContext.servletContext.contextPath}/controller?command=putMoney" method="post">
		<div><fmt:message key="top_up_100" bundle="${ rb }" /></div>
		<input type="hidden" name="summ" value="100">
		<div>
        <input type="submit" value="<fmt:message key="top_up" bundle="${ rb }" />"/>
        </div>
        </form>
<form action="${pageContext.servletContext.contextPath}/controller?command=putMoney" method="post">
		<div><fmt:message key="top_up_500" bundle="${ rb }" /></div>
		<input type="hidden" name="summ" value="500">
		<div>
        <input type="submit" value="<fmt:message key="top_up" bundle="${ rb }" />"/>
        </div>
        </form>
<form action="${pageContext.servletContext.contextPath}/controller?command=putMoney" method="post">
		<div><fmt:message key="top_up_1000" bundle="${ rb }" /></div>
		<input type="hidden" name="summ" value="1000">
		<div>
        <input type="submit" value="<fmt:message key="top_up" bundle="${ rb }" />"/>
        </div>
        </form>
</div>
<%@ include file="../jspfragments/footer.jsp" %>
