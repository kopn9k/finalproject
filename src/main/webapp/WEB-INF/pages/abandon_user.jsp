<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" language ="java"%>
<%@ include file="../jspfragments/header.jsp" %>
<div id="main">
<h2><fmt:message key="abandon" bundle="${ rb }" /></h2>
</div>
  <div id="sections">
    <h2><fmt:message key="navigation" bundle="${ rb }" /></h2>
    <ul>
      <li><a href="${pageContext.servletContext.contextPath}/controller?command=goLoginPage"><fmt:message key="login" bundle="${ rb }" /></a>
    </ul>
  </div>

</div>

<div id="meta">
  <p><fmt:message key="producer" bundle="${ rb }" />, &copy; 2018
  <p><fmt:message key="footer" bundle="${ rb }" /> +375445754172
</div>
</body>
</html>