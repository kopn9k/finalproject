<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" language ="java"%>
<%@ include file="../jspfragments/header.jsp" %>
  <div id="main">
<c:choose>
    <c:when test="${sessionScope.user.isAdmin eq 'true'}">
    <h2><fmt:message key="game_list" bundle="${ rb }" /></h2>
    <table>
        <tr>
            <th><fmt:message key="author" bundle="${ rb }" /></th>
            <th><fmt:message key="bank" bundle="${ rb }" /></th>
            <th><fmt:message key="win" bundle="${ rb }" /></th>
            <th><fmt:message key="date" bundle="${ rb }" /></th>
        </tr>
            <ctg:Show_games games="${gamesForShow}" page="${page}"/>
    </table>
    </c:when>
    <c:otherwise>
        <h2><fmt:message key="shtoss" bundle="${ rb }" /></h2>
        <div><p><fmt:message key="gameOn" bundle="${ rb }" />
            <p><fmt:message key="gameRule" bundle="${ rb }" />
            <br>
            <p><fmt:message key="gameFormat" bundle="${ rb }" />
            <br>
            <fmt:message key="gameExample" bundle="${ rb }" />
        </div>
        <c:set var="user" value="${sessionScope.user.getId()}" scope="page"/>
        <form action="${pageContext.servletContext.contextPath}/controller?command=playGame&user=${user}" method="post">
            <label title="bank"><fmt:message key="bet_amount" bundle="${ rb }" />
                <input type="number" name="bank" min="0" required/>
            </label>
            <label title="card"><fmt:message key="card" bundle="${ rb }" />
                <input type="text" name="card" required/>
            </label>
            <input type="submit" value="Enter"/>
        </form>
    </c:otherwise>
</c:choose>
</div>
<%@ include file="../jspfragments/footer.jsp" %>

