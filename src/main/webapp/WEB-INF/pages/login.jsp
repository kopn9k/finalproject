<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" language ="java"%>
<%@ include file="../jspfragments/header.jsp" %>
<div id="main">
<form action="${pageContext.servletContext.contextPath}/controller?command=login" method="post">
        <div>
        <label title="login"><fmt:message key="login" bundle="${ rb }" /></label>
        <input type="text" name="login" required/>
        </div>
        <div>
        <label title="login"><fmt:message key="password" bundle="${ rb }" /></label>
        <input type="password" name="password" required/>
        </div>
        <div>
        <input type="submit" value="<fmt:message key="enter" bundle="${ rb }" />" />
        </div>
        </form>
</div>
  <div id="sections">
    <h2><fmt:message key="navigation" bundle="${ rb }" /></h2>
  </div>

</div>

<div id="meta">
  <p><fmt:message key="producer" bundle="${ rb }" />, &copy; 2018
  <p><fmt:message key="footer" bundle="${ rb }" /> +375445754172
</div>
</body>
</html>
