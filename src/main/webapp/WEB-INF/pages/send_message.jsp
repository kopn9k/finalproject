<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" language ="java"%>
<%@ include file="../jspfragments/header.jsp" %>
  <div id="main">
    <h2><fmt:message key="send_message" bundle="${ rb }" /></h2>
        <form action="${pageContext.servletContext.contextPath}/controller?command=sendMessage" method="post">
            <label title="message"><fmt:message key="message" bundle="${ rb }" />:
                <input type="text" name="message" maxlength="255" required/>
            </label>
            <label title="recipient"><fmt:message key="recipient" bundle="${ rb }" />:
                <input type="text" name="recipient" required/>
            </label>
            <input type="submit" value="Send"/>
        </form>
</div>
<%@ include file="../jspfragments/footer.jsp" %>
