<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" language ="java"%>
<%@ include file="../jspfragments/header.jsp" %>
  <div id="main">
<c:choose>
    <c:when test="${sessionScope.user.isAdmin eq 'false'}">
    <h2><fmt:message key="news_list" bundle="${ rb }" /></h2>
        <ctg:Show_news news="${news}" page="${page}"/>
    </c:when>
    <c:otherwise>
    <h2><fmt:message key="publish_news" bundle="${ rb }" /></h2>
        <form action="${pageContext.servletContext.contextPath}/controller?command=insertNews" method="post">
            <label title="login"><fmt:message key="text" bundle="${ rb }" />:
                <input type="text" name="text" maxlength="255" required/>
            </label>
            <label title="Author"><fmt:message key="author" bundle="${ rb }" />:
                <input type="text" name="author" required/>
            </label>
            <input type="submit" value="Enter" />
        </form>
    </c:otherwise>
</c:choose>
</div>
<%@ include file="../jspfragments/footer.jsp" %>
