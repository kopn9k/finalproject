<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" language ="java"%>
<%@ include file="../jspfragments/header.jsp" %>
  <div id="main">
<h2><fmt:message key="user_list" bundle="${ rb }" /></h2>
    <table>
        <tr>
            <th><fmt:message key="name" bundle="${ rb }" /></th>
            <th><fmt:message key="surname" bundle="${ rb }" /></th>
            <th><fmt:message key="login" bundle="${ rb }" /></th>
            <th><fmt:message key="is_blocked" bundle="${ rb }" /></th>
        </tr>
        <c:forEach var="elem" items="${users}" varStatus="status">
            <tr>
                <td><c:out value="${ elem.getName() }"/></td>
                <td><c:out value="${ elem.getSurname() }"/></td>
                <td><c:out value="${ elem.getLogin() }"/></td>
                <td><c:out value="${ elem.isBlocked() }"/></td>
            </tr>
        </c:forEach>
    </table>
    <h2><fmt:message key="block_unblock_user" bundle="${ rb }" /></h2>
        <form action="${pageContext.servletContext.contextPath}/controller?command=blockUser" method="post">
                <div>
                <label title="login"><fmt:message key="login" bundle="${ rb }" /></label>
                <input type="text" name="login" required/>
                </div>
                <div>
                <label title="status"><fmt:message key="block?" bundle="${ rb }" /></label>
                <input type="text" name="block" required/>
                </div>
        		<div>
                <input type="submit" name="button" value="change_status"/>
                </div>
                </form>
    <h2><fmt:message key="add_user" bundle="${ rb }" /></h2>
    <form action="${pageContext.servletContext.contextPath}/controller?command=addUser" method="post">
                    <div>
                    <label title="name"><fmt:message key="name" bundle="${ rb }" /></label>
                    <input type="text" name="name" required/>
                    </div>
                    <div>
                    <label title="surname"><fmt:message key="surname" bundle="${ rb }" /></label>
                    <input type="text" name="surname" required/>
                    </div>
                    <div>
                    <label title="login"><fmt:message key="login" bundle="${ rb }" /></label>
                    <input type="text" name="login" required/>
                    </div>
                    <div>
                    <label title="password"><fmt:message key="password" bundle="${ rb }" /></label>
                    <input type="password" name="password" required/>
                    </div>
                    <div>
                    <label title="isAdmin"><fmt:message key="is_admin" bundle="${ rb }" /></label>
                    <input type="text" name="isAdmin" required/>
                    </div>
            		<div>
                    <input type="submit" name="button" value="Add"/>
                    </div>
                    </form>
</div>
<%@ include file="../jspfragments/footer.jsp" %>
