<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" language ="java"%>
<%@ include file="../jspfragments/header.jsp" %>
<div id="main">
<h2><fmt:message key="abandon" bundle="${ rb }" /></h2>
</div>
  <div id="sections">
    <h2><fmt:message key="navigation" bundle="${ rb }" /></h2>
    <ul>
      <c:if test="${sessionScope.user != null}">
      <li><a href="${pageContext.servletContext.contextPath}/controller?command=goHome"><fmt:message key="home" bundle="${ rb }" /></a>
      </c:if>
      <c:if test="${sessionScope.user eq null}">
      <li><a href="${pageContext.servletContext.contextPath}/controller?command=goLoginPage"><fmt:message key="login" bundle="${ rb }" /></a>
      </c:if>
    </ul>
  </div>

</div>

<div id="meta">
  <p><fmt:message key="producer" bundle="${ rb }" />, &copy; 2018
  <p><fmt:message key="footer" bundle="${ rb }" /> +375445754172
</div>
</body>
</html>