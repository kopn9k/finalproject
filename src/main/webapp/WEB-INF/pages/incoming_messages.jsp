<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" language ="java"%>
<%@ include file="../jspfragments/header.jsp" %>
  <div id="main">
    <h2>Incoming Messages</h2>
    <table>
        <tr>
            <th><fmt:message key="author" bundle="${ rb }" /></th>
            <th><fmt:message key="recipient" bundle="${ rb }" /></th>
            <th><fmt:message key="message" bundle="${ rb }" /></th>
            <th><fmt:message key="date" bundle="${ rb }" /></th>
        </tr>
            <ctg:Show_incoming_messages messages="${messages}" page="${page}"/>
    </table>
    </div>
<%@ include file="../jspfragments/footer.jsp" %>
