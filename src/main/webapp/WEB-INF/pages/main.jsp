<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" language ="java"%>
<%@ include file="../jspfragments/header.jsp" %>
<div id="main">
<h2><fmt:message key="main_head" bundle="${ rb }" /></h2>
    <p><fmt:message key="main_text" bundle="${ rb }" />
</div>
<%@ include file="../jspfragments/footer.jsp" %>
