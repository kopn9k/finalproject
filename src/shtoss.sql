create database if not exists Shtoss; -- создаем базу данных в которой будут 2 таблички, для юзеров и для игр
use Shtoss;
create table if not exists Shtoss.users ( -- создание таблички для украшений
    id int(11) not null primary key AUTO_INCREMENT,
    name varchar(50) not null,
    surname varchar(50) not null,
    login varchar(50) not null unique,
    is_admin boolean  not null,
    password varchar(50) not null,
    is_blocked boolean not null,
    money decimal(50,2) not null,
    rating int(12) not null
);
drop table shtoss.users;
select * from users;
insert into Shtoss.users ( name, surname, login, is_admin, password, is_blocked, money, rating)
values ('User', 'User', 'user', 0, 'user', false, 1500, 1500);
insert into Shtoss.users ( name, surname, login, is_admin, password, is_blocked, money, rating)
values ( 'Admin', 'Admin', 'admin', 1, 'admin', false, 0, 0);
update shtoss.users set users.is_blocked = true where users.login = 'user';
update shtoss.users set users.money = 1200, users.rating = 1200 where users.login = 'user';

select
select * from Shtoss.users where login = '' or 1 = 1; and `password` = ? or 1 = 1 ;
select * from Shtoss.users where login = '' or 1 = 1 and `password` = '' or 1 = 1 ;


create table if not exists Shtoss.news ( -- создание таблички для типов украшений
    id int(11) not null primary key AUTO_INCREMENT,
    news_text varchar(255) not null,
    date_of_publishing DATE not null,
    author_id int(11) references users(id)
);
use Shtoss

insert into Shtoss.news (news_text, date_of_publishing, author_id)
values ('Yesterday, a 1000 game was played on our website, so the administration gives all players 10$ on their accounts!', '2018.01.10', 2);,
select * from shtoss.news;
insert into Shtoss.news (news_text, date_of_publishing, author_id)
values ('Dear users 11/02/2018 on our website from 6 to 7 am will be carried out technical work. We apologize for the inconvenience', '2018.02.10', 2);
insert into Shtoss.news (news_text, date_of_publishing, author_id)
values ('Yesterday, 1000 users were registered on our site. Our team thanks all users, and promises that we will do everything to make the project better','2018.04.15', 2);
insert into Shtoss.news (news_text, date_of_publishing, author_id)
values ('It is time summarize our first half year game. All users who hit the top 10 will receive valuable prizes from the site administration','2018.05.31', 2);
insert into Shtoss.news (news_text, date_of_publishing, author_id)
values ('To the attention of players, only during the current day when you replenish the balance you get + 100% to the account','2018.07.08', select id from Shtoss.users where login=);
select * from shtoss.news;
drop table shtoss.news;

select news.id, news.news_text, users.login, news.date_of_publishing from shtoss.news inner join shtoss.users on shtoss.users.id = shtoss.news.author_id order by date_of_publishing desc, id desc;
INSERT INTO shtoss.news (news_text, date_of_publishing, author_id) SELECT
'To the attention of players, only during the current day when you replenish the balance you get + 100% to the account' ,
'2018.07.08',
id FROM users WHERE login = 'admin';
drop table shtoss.games;
create table if not exists Shtoss.games ( -- создание таблички для типов украшений
    id int(11) not null primary key AUTO_INCREMENT,
    user_id int(255) not null references users(id),
    bank DECIMAL(50,2) not null,
    win boolean not null,
    start_date DATE not null
);
insert into Shtoss.games (user_id, bank, win, start_date)
values (1, 200, true, '2018.03.29');
insert into Shtoss.games (user_id, bank, win, start_date)
values (1, 200.50, false, '2018.07.13');
insert into Shtoss.games (user_id, bank, win, start_date)
values (1, 100.50, false, '2018.09.17');
insert into Shtoss.games (user_id, bank, win, start_date)
values (1, 400, true, '2018.09.21');
insert into Shtoss.games (user_id, bank, win, start_date)
values (1, 50.50, false, '2018.10.01');

select * from shtoss.games;
select login as name from shtoss.users;

select games.id, users.login, games.bank, games.win, games.start_date from shtoss.games inner join shtoss.users on shtoss.users.id = shtoss.games.user_id order by start_date desc, id desc;

create table if not exists Shtoss.messages ( -- создание таблички для типов украшений
    id int(11) not null primary key AUTO_INCREMENT,
    user_id_from int(11) not null references users(id),
    user_id_to int not null references users(id),
    message varchar(255) not null,
    send_date DATE not null
);
select messages.id, u1.login as author, u2.login as recipient, messages.message, messages.send_date  from shtoss.messages inner join shtoss.users u1 on u1.id = shtoss.messages.user_id_from inner join shtoss.users u2 on u2.id = shtoss.messages.user_id_to order by send_date desc, id desc;
insert into Shtoss.messages (user_id_from, user_id_to, message, send_date)
values (1, 2, 'Hello!', '2018.03.29');
insert into Shtoss.messages (user_id_from, user_id_to, message, send_date)
values (2, 1, 'Hello!', '2018.03.29');
insert into Shtoss.messages (user_id_from, user_id_to, message, send_date)
values (1, 2, 'How are you?', '2018.03.29');
insert into Shtoss.messages (user_id_from, user_id_to, message, send_date)
values (2, 1, 'Good, thank you!', '2018.03.29');
insert into Shtoss.messages (user_id_from, user_id_to, message, send_date)
values (1, 2, 'Buy!', '2018.03.29');
insert into Shtoss.messages (user_id_from, user_id_to, message, send_date)
values (2, 1, 'Buy!', '2018.03.29');

insert into Shtoss.messages (user_id_from, user_id_to, message, send_date)
values (1, 2, 'Hello!', '2018.03.29');

INSERT INTO shtoss.messages (user_id_from, message, send_date, user_id_to) SELECT
1,  'Hello', '2018.07.08',
id FROM users WHERE login = 'admin';