import com.epam.shtoss.connection.ConnectionFactory;
import com.epam.shtoss.connection.ConnectionPool;
import com.epam.shtoss.exceptions.ConnectionException;
import com.epam.shtoss.exceptions.ConnectionPoolException;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Properties;

public class ConnectionPoolTest {

    public final static String PROPERTY_PATH = "/config.properties";
    public final static String URL = "db.url";
    public final static String USER = "db.login";
    public final static String PASSWORD = "db.password";
    public final static String CORRECT_URL = "jdbc:mysql://localhost:3306/Shtoss?autoReconnect=true&useSSL=false";
    public final static String CORRECT_LOGIN = "root";
    public final static String CORRECT_PASSWORD = "root";

    @Test
    public void shouldReturnCorrectParametersWhenPropertyPathComes() throws ConnectionPoolException, ConnectionException {
        //given
        ConnectionFactory connectionFactory = new ConnectionFactory();
        //when
        Properties result = connectionFactory.readProperties(PROPERTY_PATH);
        //then
        Assert.assertEquals(3,result.size());
        Assert.assertEquals(CORRECT_URL, result.get(URL));
        Assert.assertEquals(CORRECT_LOGIN, result.get(USER));
        Assert.assertEquals(CORRECT_PASSWORD, result.get(PASSWORD));
    }

}
