import com.epam.shtoss.connection.ConnectionPool;
import com.epam.shtoss.exceptions.ConnectionException;
import com.epam.shtoss.exceptions.ConnectionPoolException;
import com.epam.shtoss.logic.GameSimulator;
import com.epam.shtoss.logic.ValueGenerator;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

public class GameSimulatorTest {

    public final static String CARD = "6D";
    public final static Integer VALUE = 0;

    @Test
    public void shouldReturnFalseWhenValueGeneratorMock() throws ConnectionPoolException, ConnectionException {
        //given
        ValueGenerator mockValueGenerator = Mockito.mock(ValueGenerator.class);
        GameSimulator gameSimulator = new GameSimulator(mockValueGenerator);
        //when
        Mockito.when(mockValueGenerator.generateValue()).thenReturn(VALUE);
        //then
        boolean result = gameSimulator.playAGame(CARD);
        Assert.assertFalse(result);
    }

}
